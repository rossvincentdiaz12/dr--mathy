﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

	public GameObject MainCanvas;
	public GameObject OptionCanvas;
	public GameObject Play;

	public GameObject OptionCanvasLevelSelect;

	public GameObject Optionbutton;

	public void Option()
	{
		Optionbutton.SetActive(true);
		MainCanvas.SetActive(false);
		OptionCanvas.SetActive(true);
		Play.SetActive(false);
		OptionCanvasLevelSelect.SetActive(false);

	}

	public void CloseOption()
	{

		MainCanvas.SetActive(true);
		OptionCanvas.SetActive(false);
		Play.SetActive(false);
		OptionCanvasLevelSelect.SetActive(false);

	}

	public void QuitGame()
    {

        Debug.Log("Game Exit");
        Application.Quit();

    }

	public void PlayGame()
	{

		MainCanvas.SetActive(false);
		OptionCanvas.SetActive(false);
		Play.SetActive(true);
		
		
	}

	public void OptionLvlSelect()
	{
		OptionCanvasLevelSelect.SetActive(true);
		Optionbutton.SetActive(false);
		MainCanvas.SetActive(false);
		OptionCanvas.SetActive(true);
		Play.SetActive(false);
		
		
	}
	public void  Level1()
	{
	  Application.LoadLevel("Cherrylvl1");
	}
}
