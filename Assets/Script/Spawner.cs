﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Spawner : MonoBehaviour {
	

	[SerializeField]
	private GameObject[] tetrisObjects;
	float randx;
    Vector2 whereTospawn;

	// Use this for initialization
	void Start () {
		SpawnRandom();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SpawnRandom()
	{
		int index = Random.Range(0,tetrisObjects.Length);
		Instantiate(tetrisObjects[0],transform.position,Quaternion.identity);
	}
	
}
