﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour {

	public GameObject PauseMenu;
	public Button l,r,rr,d;

	void Start () {
		Time.timeScale = 1f;
		l.interactable =true;
		r.interactable =true;
		rr.interactable =true;
		d.interactable =true;
	}
	public void PauseMethod()
	{	
		Time.timeScale = 0f;
		PauseMenu.SetActive(true);
		
		l.interactable =false;
		r.interactable =false;
		rr.interactable =false;
		d.interactable =false;

	}

	public void PauseMethodClose()
	{	
		Time.timeScale = 1f;
		PauseMenu.SetActive(false);

		l.interactable =true;
		r.interactable =true;
		rr.interactable =true;
		d.interactable =true;

	}

	public void HomeMenu()
	{
		Time.timeScale = 1f;
		 Application.LoadLevel("CherryMain");
	}

	public void  Level1()
	{
		Time.timeScale = 1f;
	  Application.LoadLevel("Cherrylvl1");
	}

}
